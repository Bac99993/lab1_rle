package com.company;


import java.io.*;

public class RLE {
    private static final int MAX_LENGTH = 127;

    private static int decodeLen(int ch){
        return (ch & 127);
    }
    private static int encodeLen(int type, int len) {
        int res = len;
        res =  (type == 1)? ((res | (1 << 7))) : (res & (~(1 << 7)));
        return res;
    }

    private static int getType(int ch){
        return (ch >> 7) & 1;
    }

    public static void decomp(InputStream in, OutputStream out) {
        int bt;
        BufferedInputStream bin = new BufferedInputStream(in);
        try {
            while ((bt = bin.read()) != -1) {
                int type = getType(bt);
                int len = decodeLen(bt);
                if (type == 0) {
                    bt = bin.read();
                    for (int j = 0; j < len; ++j)
                        out.write(bt);
                } else {
                    for (int j = 0; j < len; ++j) {
                        bt = bin.read();
                        out.write(bt);
                    }
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public static void comp(InputStream in, OutputStream out) {
        byte[] buf = new byte[MAX_LENGTH];
        byte[] result = new byte[MAX_LENGTH*2+2];
        BufferedInputStream bin = new BufferedInputStream(in);
        try {
            int byteLength = bin.read(buf);
            while(byteLength > 0){
                int tail = 0;
                for (int i = 0; i < byteLength; ) {
                    int len = 1;
                    while (len < MAX_LENGTH && (i + len < byteLength && buf[i] == buf[i + len])) {
                        ++len;
                    }
                    if (len != 1) {
                        result[tail] = (byte)encodeLen(0 , len);
                        ++tail;
                        result[tail] = buf[i];
                        ++tail;
                        i += len;
                    }
                    len = 0;
                    while (len < MAX_LENGTH
                            && (i + len == byteLength - 1
                            || (i + len + 1  < byteLength && buf[i + len] != buf[i + len + 1]))) {
                        ++len;
                    }
                    if (len != 0) {
                        result[tail] = (byte)encodeLen(1, len);
                        ++tail;
                        for (int j = tail; j < tail + len; ++j) {
                            result[j] = buf[i + j - tail];
                        }
                        tail += len;
                        i += len;
                    }
                }
                out.write(result, 0, tail);
                byteLength = bin.read(buf);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
