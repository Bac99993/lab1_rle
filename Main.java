package com.company;
import org.apache.commons.cli.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Main {

    public static void main(String[] args) {
	Options opt = new Options();
	Option inputOpt = new Option("i","input", true, "input file path");
	opt.addOption(inputOpt);

    Option outputOpt = new Option("o","output", true, "output file path");
    opt.addOption(outputOpt);

    Option decompOpt = new Option("d","decompress", false, "decompress mode");
    opt.addOption(decompOpt);

    Option helpOpt = new Option("h","help", false, "help");
    opt.addOption(helpOpt);


    CommandLineParser parser = new DefaultParser();
    CommandLine cmd;

    try{
        cmd = parser.parse(opt, args);
    } catch (ParseException e) {
        System.err.println(e.getMessage());
        return;
    }

    if(!cmd.hasOption("input")){
        System.err.println("No input file");
        return;
    }

    if(!cmd.hasOption("output")){
        System.err.println("No output file");
        return;
    }

    if(cmd.hasOption("help")){
        HelpFormatter help = new HelpFormatter();
        help.printHelp("java -jar RleCoder.jar",opt);
        return;
    }

    try{
        String inputFilePath = cmd.getOptionValue("input");
        FileInputStream in = new FileInputStream(new File(inputFilePath));

        String outputFilePath = cmd.getOptionValue("output");
        File outFile = new File(outputFilePath);

        if(!outFile.exists()){
            outFile.createNewFile();
        }
        FileOutputStream out = new FileOutputStream(outFile);

        if(cmd.hasOption("decompress")){
            RLE.decomp(in,out);
        }
        else {
            RLE.comp(in,out);
        }

    } catch(IOException e){
            System.err.println("Exeption: "+e.getMessage());
            return;
        }

    }
}
